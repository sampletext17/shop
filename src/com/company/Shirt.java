package com.company;

import java.time.LocalDateTime;
import java.util.*;

public class Shirt extends Product {
    HashMap hashMap;

    Shirt(HashMap hm) {
        this.id = UUID.randomUUID();
        this.hashMap = hm;
        hashMap.put(LocalDateTime.now(),this);
    }

    Shirt(String n, int p, String m, String c, HashMap hm) {
        this.id = UUID.randomUUID();
        this.hashMap = hm;
        this.name = n;
        this.price = p;
        this.manufacturer = m;
        this.country = c;
    }

    @Override
    public void create() {
        id = UUID.randomUUID();
        Random random = new Random();
        String names[] = {"Red", "Swag", "Like", "Ohh", "Witch"};
        String manufacturers[] = {"Hilfiger", "Nike", "Armani", "Ralph Lauren", "Zara"};
        String countries[] = {"USA", "Russia", "Germany", "Japan", "China"};
        name = names[random.nextInt(5)];
        price = 1000 + random.nextInt(5000 - 1000);
        num++;
        manufacturer = manufacturers[random.nextInt(5)];
        country = countries[random.nextInt(5)];
    }

    @Override
    public void read() {
        System.out.println(
                "id: " + id + "\n" +
                        "Name: " + name + "\n" +
                        "Price: " + price + "\n" +
                        "Quantity: " + num + "\n" +
                        "Manufacturer: " + manufacturer + "\n" +
                        "Country: " + country + "\n");
    }

    @Override
    public void update() {
        Scanner in = new Scanner(System.in);
        id = UUID.randomUUID();
        System.out.println("Print name: ");
        name = in.nextLine();
        System.out.println("Print manufacturer: ");
        manufacturer = in.nextLine();
        System.out.println("Print country: ");
        country = in.nextLine();
        System.out.println("Print price: ");
        price = in.nextInt();
        num++;
    }

    @Override
    public void delete() {
        id = new UUID(0, 0);
        name = "";
        price = 0;
        num--;
        manufacturer = "";
        country = "";
    }
}
