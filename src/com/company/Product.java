package com.company;

import java.util.UUID;

public abstract class Product implements ICrudAction {
    UUID id;
    String name;
    int price;
    static int num;
    String manufacturer;
    String country;
}
