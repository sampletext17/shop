package com.company;

import java.time.LocalDateTime;
import java.util.LinkedList;

public class Orders {
    LinkedList<Order> linkedList = new LinkedList();

    void buy(ShoppingCart sc) {
        Order order = new Order(sc);
        linkedList.add(order);
        order.show();
        System.out.println("Order is made");
    }

    void show() {
        for (Order o: linkedList) {
            o.show();
        }
    }

    void check() {
        for (Order o: linkedList) {
            long interval = o.timeWait.getDays();
            if (o.timeStart.plusDays(interval).isAfter(LocalDateTime.now()) && o.status == "Finished") {
                linkedList.remove(o);
            }
        }
    }
}
