package com.company;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;
import java.util.UUID;

public class ShoppingCart<T> {
    ArrayList<T> arrayList = new ArrayList();
    TreeSet<UUID> treeSet = new TreeSet();
    HashMap<LocalDateTime, T> hashMap = new HashMap();

    public ShoppingCart() {

    }

    public ShoppingCart(ArrayList<T> arrayList, TreeSet<UUID> treeSet, HashMap<LocalDateTime, T> hashMap) {
        this.arrayList = arrayList;
        this.treeSet = treeSet;
        this.hashMap = hashMap;
    }

    public void add(T o) {
        arrayList.add(o);
        if (o instanceof Cap) {
            treeSet.add(((Cap) o).id);
        }
        if (o instanceof Shirt) {
            treeSet.add(((Shirt) o).id);
        }
    }

    public void delete(T o) {
        arrayList.remove(o);
    }

    public void show() {
        for (T o : arrayList) {
            if (o instanceof Cap)
                ((Cap) o).read();
            if (o instanceof Shirt)
                ((Shirt) o).read();
        }
    }

    public void showTree() {
        System.out.println("TreeSet: \n" + treeSet);
    }

    public void showHashMap() {
        System.out.println("HashMap: \n" + hashMap);
    }

    void search(UUID id) {
        if (treeSet.contains(id)) {
            System.out.println("Found id: " + id);
            for (T o : arrayList) {
                if (o instanceof Cap && ((Cap) o).id == id) {
                    ((Cap) o).read();
                }
                if (o instanceof Shirt && ((Shirt) o).id == id) {
                    ((Shirt) o).read();
                }
            }
        }
    }
}
