package com.company;

import java.util.UUID;

public class Credentials {
    UUID id;
    String surname;
    String name;
    String patronymic;
    String mail;

    public Credentials() {
        this.id = UUID.randomUUID();
        this.surname = "";
        this.name = "";
        this.patronymic = "";
        this.mail = "";
    }

    public Credentials(UUID id, String surname, String name, String patronymic, String mail) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.mail = mail;
    }


}
