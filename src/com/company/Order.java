package com.company;

import java.time.LocalDateTime;
import java.time.Period;
import java.util.Random;

public class Order {
    String status;
    LocalDateTime timeStart;
    Period timeWait;
    ShoppingCart shoppingCart;
    Credentials credentials;

    Order(ShoppingCart sc) {
        this.credentials = new Credentials();
        this.status = "Awaiting";
        this.timeStart = LocalDateTime.now();
        Random random = new Random();
        this.timeWait = Period.ofDays(random.nextInt(5));
        this.shoppingCart = sc;
    }

    Order(Credentials credentials, String st, LocalDateTime ts, Period tw, ShoppingCart sc) {
        this.credentials = credentials;
        this.status = st;
        this.timeStart = ts;
        this.timeWait = tw;
        this.shoppingCart = sc;
    }

    void show() {
        System.out.println(
                "Status: " + status + "\n" +
                        "Time start: " + timeStart + "\n" +
                        "Time wait: " + timeWait.getDays() + " seconds" + "\n" +
                        "Products: " + "\n");
        shoppingCart.show();
    }
}
