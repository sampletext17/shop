package com.company;

import java.util.HashMap;
import java.util.Scanner;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {
       /* System.out.println("Print quantity and type of products: ");
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();
        String type = in.next();

        if (type.equals("Shirt")) {
            Shirt shirt = new Shirt();
            for (int i = 0; i < number; i++) {
                shirt.update();
                shirt.read();
            }
        }
        if (type.equals("Cap")) {
            Cap cap = new Cap();
            for (int i = 0; i < number; i++) {
                cap.update();
                cap.read();
            }
        }*/
        ShoppingCart<Product> shoppingCart = new ShoppingCart<Product>();
        Shirt shirt = new Shirt(shoppingCart.hashMap);
        shirt.create();
        Cap cap = new Cap(shoppingCart.hashMap);
        cap.create();
        shoppingCart.add(shirt);
        shoppingCart.add(cap);
        Cap cap1 = new Cap(shoppingCart.hashMap);
        cap1.create();
        shoppingCart.add(cap1);
        System.out.println(shoppingCart.arrayList.size());
        shoppingCart.show();
        Orders orders = new Orders();
        orders.buy(shoppingCart);
        shoppingCart.showTree();
        UUID idTest = cap.id;
        shoppingCart.search(idTest);
        shoppingCart.showHashMap();
    }
}
